import { Module } from '@nestjs/common';
import { PostCoreModule } from '@core/post/post.core-module';
import { PostEventsLoggerAppModule } from '@app/post-events-logger/post-events-logger.app-module';
import { PostEventsProxyAppModule } from '@app/post-events-proxy/post-events-proxy.app-module';
import { ConfigInfraModule } from '@infra/config/config.infra-module';
import { EventBusCoreModule } from '@core/event-bus/event-bus.core-module';
import { PostEventsBroadcastAdapterModule } from '@adapters/outbound/post-events-broadcast/post-events-broadcast.adapter-module';
import { RabbitmqInfraModule } from '@infra/rabbitmq/rabbitmq.infra-module';

@Module({
  imports: [
    /* The Core */
    EventBusCoreModule,
    PostCoreModule,

    /* Application logic */
    PostEventsLoggerAppModule,
    PostEventsProxyAppModule,

    /* Outbound adapters */
    PostEventsBroadcastAdapterModule,

    /* Inbound adapters */

    /* Infrastructure */
    ConfigInfraModule,
    RabbitmqInfraModule,
  ],
})
export class AppModule {}
