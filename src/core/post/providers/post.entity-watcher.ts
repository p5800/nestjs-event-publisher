import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';
import { CoreEventProducer } from '@lib/core-events/types/core-event-producer';
import { PostDeletedCoreEvent } from '@core/post/events/post-deleted.core-event';
import { PostCreatedCoreEvent } from '@core/post/events/post-created.core-event';

@Injectable()
export class PostEntityWatcher implements OnApplicationBootstrap {
  constructor(
    @Inject(CoreEventBus) private readonly eventPort: CoreEventProducer,
  ) {}

  private notifyPostCreated() {
    this.eventPort.push(
      new PostCreatedCoreEvent({
        post: {
          id: 100,
          title: 'Dummy',
          authorId: 99,
        },
      }),
    );
  }

  private notifyPostDeleted() {
    this.eventPort.push(
      new PostDeletedCoreEvent({
        post: {
          id: 101,
        },
      }),
    );
  }

  public onApplicationBootstrap(): void {
    this.setupWatcher();
  }

  private setupWatcher() {
    setInterval(() => this.notifyPostCreated(), 4700);
    setInterval(() => this.notifyPostDeleted(), 6666);
  }
}
