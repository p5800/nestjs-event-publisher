import { Module } from '@nestjs/common';
import { PostEntityWatcher } from '@core/post/providers/post.entity-watcher';

/**
 * ## Module, providing business-operations for Posts.
 * Actually does not provide anything (just demo)
 */
@Module({
  providers: [PostEntityWatcher],
})
export class PostCoreModule {}
