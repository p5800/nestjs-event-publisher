import { BaseCoreEvent } from '@lib/core-events/types/base-core-event';

/**
 * ## Event that should be fired when Post created
 */
export class PostCreatedCoreEvent extends BaseCoreEvent {
  constructor(
    public readonly payload: {
      post: {
        id: number;
        title: string;
        authorId: number;
      };
    },
  ) {
    super();
  }
}
