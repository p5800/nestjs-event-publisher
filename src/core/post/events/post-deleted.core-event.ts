import { BaseCoreEvent } from '@lib/core-events/types/base-core-event';

/**
 * ## Event that should be fired when Post deleted
 */
export class PostDeletedCoreEvent extends BaseCoreEvent {
  constructor(
    public readonly payload: {
      post: {
        id: number;
      };
    },
  ) {
    super();
  }
}
