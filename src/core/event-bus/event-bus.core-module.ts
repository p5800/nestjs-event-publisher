import { Global, Module } from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';

@Global()
@Module({
  providers: [CoreEventBus],
  exports: [CoreEventBus],
})
export class EventBusCoreModule {}
