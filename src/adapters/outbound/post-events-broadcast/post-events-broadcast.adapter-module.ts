import { Global, Module } from '@nestjs/common';
import { PostCreatedBroadcastPort } from '@app/post-events-proxy/ports/post-created.broadcast-port';
import { PostDeletedBroadcastPort } from '@app/post-events-proxy/ports/post-deleted.broadcast-port';
import { PostCreatedBroadcastAdapter } from '@adapters/outbound/post-events-broadcast/providers/post-created.broadcast-adapter';
import { PostDeletedBroadcastAdapter } from '@adapters/outbound/post-events-broadcast/providers/post-deleted.broadcast-adapter';

/**
 * ## Module, providing implementation for post broadcast ports
 * ### Requires importing
 * - Common Events Broadcaster
 */
@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: PostCreatedBroadcastPort,
      useClass: PostCreatedBroadcastAdapter,
    },
    {
      provide: PostDeletedBroadcastPort,
      useClass: PostDeletedBroadcastAdapter,
    },
  ],
  exports: [PostCreatedBroadcastPort, PostDeletedBroadcastPort],
})
export class PostEventsBroadcastAdapterModule {}
