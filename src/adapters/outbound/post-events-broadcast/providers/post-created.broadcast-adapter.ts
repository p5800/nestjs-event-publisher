import { Injectable } from '@nestjs/common';
import { InfraEventBroadcaster } from '@lib/infra-events/infra-event-broadcaster';
import { PostCreatedCoreEvent } from '@core/post/events/post-created.core-event';
import { PostCreatedBroadcastPort } from '@app/post-events-proxy/ports/post-created.broadcast-port';

@Injectable()
export class PostCreatedBroadcastAdapter extends PostCreatedBroadcastPort {
  private readonly topic = 'post-created';

  constructor(private readonly eventBroadcaster: InfraEventBroadcaster) {
    super();
  }

  public async broadcast(e: PostCreatedCoreEvent): Promise<void> {
    await this.eventBroadcaster.broadcast(this.topic, e.payload);
  }
}
