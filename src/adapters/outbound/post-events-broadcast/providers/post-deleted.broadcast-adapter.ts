import { PostDeletedBroadcastPort } from '@app/post-events-proxy/ports/post-deleted.broadcast-port';
import { Injectable } from '@nestjs/common';
import { PostDeletedCoreEvent } from '@core/post/events/post-deleted.core-event';
import { InfraEventBroadcaster } from '@lib/infra-events/infra-event-broadcaster';

@Injectable()
export class PostDeletedBroadcastAdapter extends PostDeletedBroadcastPort {
  private readonly topic = 'post-deleted';

  constructor(private readonly eventBroadcaster: InfraEventBroadcaster) {
    super();
  }

  public async broadcast(e: PostDeletedCoreEvent): Promise<void> {
    await this.eventBroadcaster.broadcast(this.topic, e.payload);
  }
}
