import { RabbitmqConfig } from '../../rabbitmq/needs/rabbitmq.config';

export class RabbitmqConfigValues extends RabbitmqConfig {
  public get url(): string {
    return process.env.RMQ_URL ?? 'amqp://127.0.0.1:5672';
  }
}
