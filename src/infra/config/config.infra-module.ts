import { Global, Module } from '@nestjs/common';
import { RabbitmqConfig } from '../rabbitmq/needs/rabbitmq.config';
import { RabbitmqConfigValues } from './providers/rabbitmq.config-values';

/**
 * ## Infrastructure module, providing configs for the rest of modules.
 */
@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: RabbitmqConfig,
      useClass: RabbitmqConfigValues,
    },
  ],
  exports: [RabbitmqConfig],
})
export class ConfigInfraModule {}
