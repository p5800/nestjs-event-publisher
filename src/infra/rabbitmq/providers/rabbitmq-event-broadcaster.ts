import { InfraEventBroadcaster } from '@lib/infra-events/infra-event-broadcaster';
import { ChannelWrapper } from 'amqp-connection-manager';
import { Inject } from '@nestjs/common';
import { ChannelWrapperInjectionToken } from '../const/channel-wrapper.injection-token';

export class RabbitmqEventBroadcaster extends InfraEventBroadcaster {
  private readonly knownTopics: Set<string> = new Set<string>();

  public constructor(
    @Inject(ChannelWrapperInjectionToken)
    private readonly channel: Readonly<ChannelWrapper>,
  ) {
    super();
  }

  public async broadcast(topic: string, data: unknown): Promise<void> {
    if (!this.knownTopics.has(topic)) {
      await this.channel.assertExchange(topic, 'fanout');
      this.knownTopics.add(topic);
    }

    await this.channel.publish(topic, '', { pattern: topic, data });
  }
}
