import { Global, Module } from '@nestjs/common';
import { InfraEventBroadcaster } from '@lib/infra-events/infra-event-broadcaster';
import { RabbitmqEventBroadcaster } from './providers/rabbitmq-event-broadcaster';
import { ChannelWrapperInjectionToken } from '@infra/rabbitmq/const/channel-wrapper.injection-token';
import { RabbitmqConfig } from '@infra/rabbitmq/needs/rabbitmq.config';
import amqp from 'amqp-connection-manager';

/**
 * ## Module, providing common event broadcaster
 * ### Requires importing
 * - RabbitmqConfig
 */
@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: ChannelWrapperInjectionToken,
      inject: [RabbitmqConfig],
      useFactory: (config: RabbitmqConfig) =>
        amqp.connect([config.url]).createChannel({
          publishTimeout: 3000,
          confirm: true,
          json: true,
          name: 'Broadcaster',
        }),
    },
    {
      provide: InfraEventBroadcaster,
      useClass: RabbitmqEventBroadcaster,
    },
  ],
  exports: [InfraEventBroadcaster],
})
export class RabbitmqInfraModule {}
