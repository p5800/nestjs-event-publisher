export abstract class InfraEventBroadcaster {
  public abstract broadcast(topic: string, data: unknown): Promise<void>;
}
