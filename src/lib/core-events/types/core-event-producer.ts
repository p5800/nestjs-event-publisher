import { BaseCoreEvent } from './base-core-event';

/**
 * ## Base class for classes that produces core infra-events
 */
export interface CoreEventProducer {
  push(e: BaseCoreEvent): void;
}
