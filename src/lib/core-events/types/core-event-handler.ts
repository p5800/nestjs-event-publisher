import { BaseCoreEvent } from './base-core-event';

/**
 * ## Abstract handler for core infra-events
 */
export type CoreEventHandler<TEvent extends BaseCoreEvent> = (e: TEvent) => unknown;
