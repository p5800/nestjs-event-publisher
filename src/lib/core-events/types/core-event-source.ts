import { CoreEventHandler } from './core-event-handler';
import { CoreEventConstructor } from './core-event-constructor';
import { BaseCoreEvent } from './base-core-event';

/**
 * ## Base class for core-infra-events listeners
 */
export interface CoreEventSource {
  on<TEvent extends BaseCoreEvent>(
    eventClass: CoreEventConstructor<TEvent>,
    handler: CoreEventHandler<TEvent>,
  ): void;
  off<TEvent extends BaseCoreEvent>(
    eventClass: CoreEventConstructor<TEvent>,
    handler: CoreEventHandler<TEvent>,
  ): void;
}
