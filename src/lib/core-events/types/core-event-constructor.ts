import { BaseCoreEvent } from './base-core-event';

export interface CoreEventConstructor<TEvent extends BaseCoreEvent> {
  new (...args: any): TEvent;
}
