import { CoreEventBus } from './core-event-bus';
import { BaseCoreEvent } from '../types/base-core-event';

class TestBaseEvent extends BaseCoreEvent {}

describe('Event bus', () => {
  it('Should notify listeners when event is emitted', () => {
    const bus = new CoreEventBus();

    const handler = jest.fn((e: BaseCoreEvent) => e);
    const reason = new TestBaseEvent();

    bus.on(TestBaseEvent, handler);
    bus.push(reason);

    expect(handler).toBeCalledWith(reason);
  });

  it('Should correctly revoke handlers', () => {
    const bus = new CoreEventBus();

    const handler = jest.fn((e: BaseCoreEvent) => e);
    const reason = new TestBaseEvent();

    bus.on(TestBaseEvent, handler);
    bus.off(TestBaseEvent, handler);
    bus.push(reason);

    expect(handler).not.toBeCalled();
  });

  it('Should notify multiple handlers on same event (if subscribed)', () => {
    const bus = new CoreEventBus();

    const handlerA = jest.fn((e: BaseCoreEvent) => e);
    const handlerB = jest.fn((e: BaseCoreEvent) => e);

    bus.on(TestBaseEvent, handlerA);
    bus.on(TestBaseEvent, handlerB);
    bus.push(new TestBaseEvent());

    expect(handlerA).toBeCalled();
    expect(handlerB).toBeCalled();
  });

  it('Should not notify handlers on inappropriate event', () => {
    class DummyEvent extends BaseCoreEvent {}

    const bus = new CoreEventBus();

    const handlerA = jest.fn((e: BaseCoreEvent) => e);
    const handlerB = jest.fn((e: BaseCoreEvent) => e);

    bus.on(TestBaseEvent, handlerA);
    bus.on(DummyEvent, handlerB);

    bus.push(new DummyEvent());

    expect(handlerA).not.toBeCalled();
    expect(handlerB).toBeCalled();
  });
});
