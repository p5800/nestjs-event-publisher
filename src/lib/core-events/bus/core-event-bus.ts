import { CoreEventProducer } from '../types/core-event-producer';
import { CoreEventSource } from '../types/core-event-source';
import { BaseCoreEvent } from '../types/base-core-event';
import { CoreEventHandler } from '../types/core-event-handler';
import { CoreEventConstructor } from '../types/core-event-constructor';

export class CoreEventBus implements CoreEventSource, CoreEventProducer {
  private subscription: Map<
    CoreEventConstructor<BaseCoreEvent>,
    CoreEventHandler<BaseCoreEvent>[]
  >;

  constructor() {
    this.subscription = new Map<
      CoreEventConstructor<BaseCoreEvent>,
      CoreEventHandler<BaseCoreEvent>[]
    >();
  }

  public push(e: BaseCoreEvent): void {
    for (const [eventType, handlers] of this.subscription.entries())
      if (e instanceof eventType) for (const handler of handlers) handler(e);
  }

  public on<TEvent extends BaseCoreEvent>(
    eventClass: CoreEventConstructor<TEvent>,
    handler: CoreEventHandler<TEvent>,
  ): void {
    const handlers = this.subscription.get(eventClass) || [];
    if (!handlers.includes(handler))
      this.subscription.set(eventClass, [...handlers, handler]);
  }

  public off<TEvent extends BaseCoreEvent>(
    eventClass: CoreEventConstructor<TEvent>,
    handler: CoreEventHandler<TEvent>,
  ): void {
    const handlers = this.subscription.get(eventClass) || [];
    const newHandlers = handlers.filter((h) => h !== handler);
    this.subscription.set(eventClass, newHandlers);
  }
}
