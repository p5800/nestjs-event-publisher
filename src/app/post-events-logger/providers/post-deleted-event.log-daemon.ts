import {
  Inject,
  Injectable,
  Logger,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';
import { CoreEventSource } from '@lib/core-events/types/core-event-source';
import { PostDeletedCoreEvent } from '@core/post/events/post-deleted.core-event';

@Injectable()
export class PostDeletedEventLogDaemon
  implements OnModuleInit, OnModuleDestroy
{
  constructor(
    @Inject(CoreEventBus) private readonly eventSource: CoreEventSource,
    private readonly logger: Logger,
  ) {}

  private readonly logPostDeleted = (e: PostDeletedCoreEvent) =>
    this.logger.log(`Post ${e.payload.post.id} deleted`);

  public onModuleInit(): void {
    this.initLogger();
  }

  public onModuleDestroy(): void {
    this.teardownLogger();
  }

  private initLogger(): void {
    this.eventSource.on(PostDeletedCoreEvent, this.logPostDeleted);
  }

  private teardownLogger(): void {
    this.eventSource.off(PostDeletedCoreEvent, this.logPostDeleted);
  }
}
