import {
  Inject,
  Injectable,
  Logger,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';
import { CoreEventSource } from '@lib/core-events/types/core-event-source';
import { PostCreatedCoreEvent } from '@core/post/events/post-created.core-event';

@Injectable()
export class PostCreatedEventLogDaemon
  implements OnModuleInit, OnModuleDestroy
{
  constructor(
    @Inject(CoreEventBus) private readonly eventSource: CoreEventSource,
    private readonly logger: Logger,
  ) {}

  private readonly logPostCreated = (e: PostCreatedCoreEvent) =>
    this.logger.log(
      `Post ${e.payload.post.id} created by ${e.payload.post.authorId}`,
    );

  public onModuleInit(): void {
    this.initLogger();
  }

  public onModuleDestroy(): void {
    this.teardownLogger();
  }

  private initLogger(): void {
    this.eventSource.on(PostCreatedCoreEvent, this.logPostCreated);
  }

  private teardownLogger(): void {
    this.eventSource.off(PostCreatedCoreEvent, this.logPostCreated);
  }
}
