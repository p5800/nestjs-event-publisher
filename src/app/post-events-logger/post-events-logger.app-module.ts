import { Logger, Module } from '@nestjs/common';
import { PostCreatedEventLogDaemon } from '@app/post-events-logger/providers/post-created-event.log-daemon';
import { PostDeletedEventLogDaemon } from '@app/post-events-logger/providers/post-deleted-event.log-daemon';

@Module({
  providers: [
    {
      // In a perfect scenario, logger comes as output-adapter.
      // It should be defined at infra layer.

      provide: Logger,
      useValue: new Logger('Post Events'),
    },
    PostCreatedEventLogDaemon,
    PostDeletedEventLogDaemon,
  ],
})
export class PostEventsLoggerAppModule {}
