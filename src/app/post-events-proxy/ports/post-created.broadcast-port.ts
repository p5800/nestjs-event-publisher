import { PostCreatedCoreEvent } from '@core/post/events/post-created.core-event';

export abstract class PostCreatedBroadcastPort {
  public abstract broadcast(e: PostCreatedCoreEvent): Promise<void>;
}
