import { PostDeletedCoreEvent } from '@core/post/events/post-deleted.core-event';

export abstract class PostDeletedBroadcastPort {
  public abstract broadcast(e: PostDeletedCoreEvent): Promise<void>;
}
