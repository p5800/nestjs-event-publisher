import { Inject, Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';
import { CoreEventSource } from '@lib/core-events/types/core-event-source';
import { PostCreatedCoreEvent } from '@core/post/events/post-created.core-event';
import { PostCreatedBroadcastPort } from '@app/post-events-proxy/ports/post-created.broadcast-port';

@Injectable()
export class PostCreatedEventProxy implements OnModuleInit, OnModuleDestroy {
  constructor(
    @Inject(CoreEventBus) private readonly eventSource: CoreEventSource,
    private readonly broadcaster: PostCreatedBroadcastPort,
  ) {}

  private readonly handlePostCreated = (e: PostCreatedCoreEvent) => this.broadcaster.broadcast(e);

  public onModuleInit(): void {
    this.initProxy();
  }

  public onModuleDestroy(): void {
    this.teardownProxy();
  }

  private initProxy(): void {
    this.eventSource.on(PostCreatedCoreEvent, this.handlePostCreated);
  }

  private teardownProxy(): void {
    this.eventSource.off(PostCreatedCoreEvent, this.handlePostCreated);
  }
}
