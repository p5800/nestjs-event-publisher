import { Inject, Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { CoreEventBus } from '@lib/core-events/bus/core-event-bus';
import { CoreEventSource } from '@lib/core-events/types/core-event-source';
import { PostDeletedBroadcastPort } from '@app/post-events-proxy/ports/post-deleted.broadcast-port';
import { PostDeletedCoreEvent } from '@core/post/events/post-deleted.core-event';

@Injectable()
export class PostDeletedEventProxy implements OnModuleInit, OnModuleDestroy {
  constructor(
    @Inject(CoreEventBus) private readonly eventSource: CoreEventSource,
    private readonly broadcaster: PostDeletedBroadcastPort,
  ) {}

  private readonly handlePostDeleted = (e: PostDeletedCoreEvent) => this.broadcaster.broadcast(e);

  public onModuleInit(): void {
    this.initProxy();
  }

  public onModuleDestroy(): void {
    this.teardownProxy();
  }

  private initProxy(): void {
    this.eventSource.on(PostDeletedCoreEvent, this.handlePostDeleted);
  }

  private teardownProxy(): void {
    this.eventSource.off(PostDeletedCoreEvent, this.handlePostDeleted);
  }
}
