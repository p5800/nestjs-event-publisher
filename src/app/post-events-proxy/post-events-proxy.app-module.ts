import { Module } from '@nestjs/common';
import { PostCreatedEventProxy } from '@app/post-events-proxy/providers/post-created.event-proxy';
import { PostDeletedEventProxy } from '@app/post-events-proxy/providers/post-deleted.event-proxy';

/**
 * ## Module, providing event listening & broadcasting to the infra.
 * ### Requires importing
 * - PostCreatedBroadcastPort implementation
 * - PostDeletedBroadcastPort implementation
 */
@Module({
  providers: [PostCreatedEventProxy, PostDeletedEventProxy],
})
export class PostEventsProxyAppModule {}
